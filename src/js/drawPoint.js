var data = (function () {
    var json = null;
    $.ajax({
        'async': false,
        'global': false,
        'url': "../../data.json",
        'dataType': "json",
        'success': function (data) {
            json = data;
        }
    });
    return json;
})();


// list location node receive from admin
let listNode
// list location node receive from admin
let listEdge
// list location node receive from admin
let listSubEdge
// setting frame of div and img to show
let frame_x;
let frame_y;

let boudaryOfNode;
let xLeft, xRight, yTop, yBot;
let serverDomain;
let apiUpdate;
let apiDeleteCache;


// setting size of point
let widthNode;
let heightNode;
let scaleNode;


let dataSavePoint;

//data post to server
let userID = "1"
let clothesKey = "https://fashion-shop-dev.s3-ap-southeast-1.amazonaws.com/private/photo/processed-product/1316_49.png?X-Amz-Algorithm=AWS4-HMAC-SHA256&X-Amz-Date=20200611T091655Z&X-Amz-SignedHeaders=host&X-Amz-Expires=86400&X-Amz-Credential=AKIA53OHPYDELK25HWSO/20200611/ap-southeast-1/s3/aws4_request&X-Amz-Signature=89479c5c11d567682c0f7072afd460192fcb4fd6140c8c773b5e762110f4eeb5";

let category = "gp02"

listNode = data[category].listNode;
listEdge = data[category].listEdge;
listSubEdge = data[category].listSubEdge;

frame_x = data.frame_x;
frame_y = data.frame_y;
serverDomain = data.serverDomain;
apiUpdate = data.apiUpdate;
apiDeleteCache = data.apiDeleteCache;
widthNode = data.widthNode;
heightNode = data.heightNode;
scaleNode = data.scaleNode;


const imgContentDislay = document.querySelector('img[id=image_display]');
const divContent = document.querySelector('div[id=divIncludePoint]');
const imgShow = document.querySelector('img[id=image_show]');


//get size original of image to calculate percent of point in image
let wi = imgShow.naturalWidth;
let hi = imgShow.naturalHeight;
let cy;


//create arr node from list points and position of point
// [x,y] is position of point correct with img
function createArrNodes(list) {
    try {
        let obj;
        let result = [];
        for (let i = 0; i < list.length; i++) {
            obj = {
                group: 'nodes',
                data: {
                    id: i.toString(),
                    width: widthNode,
                    height: heightNode,
                    scale: scaleNode
                },
                position: {
                    x: list[i][0],
                    y: list[i][1]
                }
            }
            result.push(obj);
        }
        return result;
    } catch (err) {
        console.log("Error load array nodes", err);
    }

}

// create arr edges from list edges, this is boundary of image
function createEdges(list, typeEdge) {
    try {
        let obj;
        let result = [];
        for (let i = 0; i < list.length; i++) {
            obj = {
                group: 'edges',
                data: {
                    type: typeEdge,
                    source: list[i][0].toString(),
                    target: list[i][1].toString()
                }
            }
            result.push(obj);
        }
        return result;
    } catch (err) {
        console.log("Error load array Edges", err);
    }

}

//create arr subedges from list subsedges, this is the connection inside image
function splitSubEdges(list) {
    try {
        let result = [];
        for (let i = 0; i < list.length; i++) {
            obj = {
                group: 'edges',
                data: {
                    type: typeEdge,
                    source: list[i][0].toString(),
                    target: list[i][1].toString()
                }
            }
            result.push(obj);
        }
        return result;
    }
    catch (err) {
        console.log("Error load array Sub Edges", err);
    }

}

function calculatePercent(number, frame) {
    return number / frame
}


let checkAdd = false;

//create point and connect node
function createPoints() {
    try {
        let arrLocationPoint = createArrNodes(listNode).concat(createEdges(listEdge, "1"))

        arrLocationPoint = arrLocationPoint.map(element => {
            if (element.group === 'nodes') {
                element.position.x = element.position.x * frame_x;
                element.position.y = element.position.y * frame_y;
            }
            return element;
        })

        if (!checkAdd) {
            cy = cytoscape({
                container: divContent,
                style: [
                    {
                        selector: 'nodes',
                        style: {
                            'background-color': '#A52A2A',
                            'label': 'data(id)',
                            'z-index': '3',
                            width: ele => `${ele.data("width") * ele.data("scale")}`,
                            height: ele => `${ele.data("height") * ele.data("scale")}`,
                        }
                    },
                    {
                        selector: 'edges',
                        style: {
                            width: ele => `${ele.data("type") == '1' ? 3 : 1}`,
                            'line-color': ele => `${ele.data("type") == '1' ? '#5F9EA0' : '#556B2F'}`,
                            'font-size': '14px',
                            'color': '#777',
                            'z-index': '3'
                        }
                    }

                ],
                layout: {
                    name: "grid",
                    rows: 1
                }
            });

            cy.fit();
            var eles = cy.add(arrLocationPoint);
            checkAdd = true;

            //enable panning and zooming node
            cy.userZoomingEnabled(false);
            cy.userPanningEnabled(false);
            boudaryOfNode = cy.extent();
            xLeft = boudaryOfNode.x1;
            xRight = boudaryOfNode.x2;
            yTop = boudaryOfNode.y1;
            yBot = boudaryOfNode.y2;

            let currentX, currentY;
            //event when move points(nodes)
            cy.on('mouseup', 'nodes', function (evt) {
                var node = evt.target;


                //set boudary of node, if moved outside => set new position of node equal with boudary of div content
                currentX = node.position().x;
                currentY = node.position().y;
                if (currentX > xRight) node.position().x = xRight;
                if (currentX < xLeft) node.position().x = xLeft;
                if (currentY < yTop) node.position().y = yTop;
                if (currentY > yBot) node.position().y = yBot;

                let currentElements = cy.elements().jsons()
                let resultLog = currentElements
                    .filter(e => e.group === 'nodes')
                    .map(e => {
                        return {
                            label: e.data.id,
                            x: calculatePercent(e.position.x, frame_x),
                            y: calculatePercent(e.position.y, frame_y)
                        }
                    })


                console.log(JSON.stringify(resultLog));

                //send keypoint,userID and clothesKey to server
                // receive dataSavePoint for saving to db if click save
                $.ajax({
                    type: "POST",
                    url: serverDomain + apiUpdate,
                    data: {
                        arrKeyPoint: JSON.stringify(resultLog),
                        clothesKey: clothesKey,
                        category: category
                    },
                    dataType: "json",
                }).done(function (data) {
                    console.log("Request Done");
                    if (data['error'] == 1) {
                        alert("ERROR")
                    }
                    else {
                        result = data.result['image'];
                        dataSavePoint = data.result['keypoints'];
                        //set src for image display
                        imgContentDislay.src = "data:image/png;base64," + result
                    }
  
                }).fail(function (error) {
                    console.log("error", error);
                })

            });
        }

    } catch (err) {
        console.log("Error Create Point", err);
    }

}

function SavePoints() {
    try {
        //send request to server to delete cache
        $.ajax({
            type: "POST",
            url: serverDomain + apiDeleteCache,
            data: {
                message: true,
                userID: userID,
                clothesKey: clothesKey
            },
            dataType: "json",
        }).done(function (data) {
            console.log("Request Done!!");
            if (data['error'] == 1) {
                alert("ERROR")
            }
            else {
                //update this point to db
                console.log(dataSavePoint);
            }

        }).fail(function (error) {
            console.log("error", error);
        })
    } catch (err) {
        console.log("Error Save Point", err);
    }

}
