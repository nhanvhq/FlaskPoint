const imgContentDislay = document.querySelector('img[id=imageWithForm]');

function concatData(arrData) {
    let result = "";
    let arrLength = arrData.length;
    for (let i = 0; i < arrLength - 1; i++) {
        result += arrData[i] + "_";
    }
    return result + arrData[arrLength - 1]
}

function DisplayChange(newValue, idPTag) {
    try {
        //get Name of p tag
        let tabName = idPTag.id.slice(0, idPTag.id.indexOf("Z"))
        //get number index of p tag
        let tabNumber = idPTag.id.slice(idPTag.id.indexOf("Z") + 1)
        //get display data input id
        let inputID = tabName === "txtTrunk" ? "inTrunkZ" : tabName === "txtNeck" ? "inNeckZ" : "inArmZ";

        let idInputSlide = "#" + inputID + tabNumber;

        //if new value of input range is lower than 1, set it to 0
        if (newValue < 1) {
            $(idInputSlide).val(0);
        }

        let concatTrunkData, concatNeckData, concatArmData;

        if (tabName === "txtTrunk") {
            switch (tabNumber) {
                //this case have 4 input range
                // input 1,2 is free, input 3 is depend on 2, 4 on 3
                // Reset level 
                // 2 : will set 3,4 to 0
                // 3 : 4
                case "2":
                    if ($(idInputSlide).val() == 0) {
                        $("#" + inputID + 3).prop("disabled", true);
                        $("#" + inputID + 4).prop("disabled", true);
                        $("#" + inputID + 3).val(0);
                        $("#" + inputID + 4).val(0);
                        $("#" + tabName + "Z3").text("( 0 )")
                        $("#" + tabName + "Z4").text("( 0 )")
                    }
                    else {
                        $("#" + inputID + 3).prop("disabled", false);
                    }
                    break;
                case "3":

                    if ($(idInputSlide).val() == 0) {
                        $("#" + inputID + 4).prop("disabled", true);
                        $("#" + inputID + "4").val(0);
                        $("#" + tabName + "Z4").text("( 0 )")
                    }
                    else {
                        $("#" + inputID + 4).prop("disabled", false);
                    }
                    break;
            }

            concatTrunkData = concatData([
                $("#" + inputID + 1).val(), $("#" + inputID + 2).val(),
                $("#" + inputID + 3).val(), $("#" + inputID + 4).val()
            ]);
            $("#trunkDataID").val(concatTrunkData);

        } else if (tabName === "txtArm") {
            switch (tabNumber) {
                //this case have 6 input range
                // input 1,2 is free, input 3 is depend on 2, 4 on 3, 5 on 4, 6 on 5
                // Reset level 
                // 2 : will set 3,4,5,6 to 0
                // 3 : 4,5,6
                // 4 : 5,6
                // 5: 6
                case "2":
                    if ($(idInputSlide).val() == 0) {
                        $("#" + inputID + 3).prop("disabled", true);
                        $("#" + inputID + 4).prop("disabled", true);
                        $("#" + inputID + 5).prop("disabled", true);
                        $("#" + inputID + 6).prop("disabled", true);
                        $("#" + inputID + 3).val(0);
                        $("#" + inputID + 4).val(0);
                        $("#" + inputID + 5).val(0);
                        $("#" + inputID + 6).val(0);
                        $("#" + tabName + "Z3").text("( 0 )")
                        $("#" + tabName + "Z4").text("( 0 )")
                        $("#" + tabName + "Z5").text("( 0 )")
                        $("#" + tabName + "Z6").text("( 0 )")
                    }
                    else {
                        $("#" + inputID + 3).prop("disabled", false);
                    }
                    break;
                case "3":
                    if ($(idInputSlide).val() == 0) {
                        $("#" + inputID + 4).prop("disabled", true);
                        $("#" + inputID + 5).prop("disabled", true);
                        $("#" + inputID + 6).prop("disabled", true);
                        $("#" + inputID + 4).val(0);
                        $("#" + inputID + 5).val(0);
                        $("#" + inputID + 6).val(0);
                        $("#" + tabName + "Z4").text("( 0 )")
                        $("#" + tabName + "Z5").text("( 0 )")
                        $("#" + tabName + "Z6").text("( 0 )")
                    }
                    else {
                        $("#" + inputID + 4).prop("disabled", false);
                    }
                    break;
                case "4":
                    if ($(idInputSlide).val() == 0) {
                        $("#" + inputID + 5).prop("disabled", true);
                        $("#" + inputID + 6).prop("disabled", true);
                        $("#" + inputID + 5).val(0);
                        $("#" + inputID + 6).val(0);
                        $("#" + tabName + "Z5").text("( 0 )")
                        $("#" + tabName + "Z6").text("( 0 )")
                    }
                    else {
                        $("#" + inputID + 5).prop("disabled", false);
                    }
                    break;
                case "5":
                    if ($(idInputSlide).val() == 0) {
                        $("#" + inputID + 6).prop("disabled", true);
                        $("#" + inputID + 6).val(0);
                        $("#" + tabName + "Z6").text("( 0 )")
                    }
                    else {
                        $("#" + inputID + 6).prop("disabled", false);
                    }
                    break;
            }
            concatArmData = concatData([
                $("#" + inputID + 1).val(), $("#" + inputID + 2).val(),
                $("#" + inputID + 3).val(), $("#" + inputID + 4).val(),
                $("#" + inputID + 5).val(), $("#" + inputID + 6).val()
            ]);
            $("#armDataID").val(concatArmData);

        }
        else {
            //Neck data level 1,2 is free
            concatNeckData = concatData([
                $("#" + inputID + 1).val(), $("#" + inputID + 2).val()
            ]);
            $("#neckDataID").val(concatNeckData);
        }

        //set value for p tab value correct with the input range which is changing
        $(idPTag).text("( " + $(idInputSlide).val() + " )")

        //prepare data send to server
        let valTrunkData = $("#trunkDataID").val();
        let valNeckData = $("#neckDataID").val();
        let valArmData = $("#armDataID").val();

        //send userID and new code form to server to create new category
        $.ajax({
            type: "POST",
            url: "http://192.168.9.55:5000/api/v1/create_form",
            data: {
                codeForm: "TOP" + valTrunkData + "_" + valNeckData + "x" + valArmData,
                userID: "1"
            },
            dataType: "json",
        }).done(function (data) {
            console.log("Request Done");
            if (data['error'] == 1) {
                alert("ERROR")
            }
            else {
                result = data.result['image'];
                dataSavePoint = data.result['keypoints'];

                //set src for image display
                imgContentDislay.src = "data:image/png;base64," + result
            }

        }).fail(function (error) {
            console.log("error", error);
        })
    } catch (err) {
        console.log("Error Create Point", err);
    }
}

function Saveform() {
    try {
        //send request to server to save form
        $.ajax({
            type: "POST",
            url: "http://192.168.9.55:5000/save_form",
            data: {
                message: true,
                userID: "1",
            },
            dataType: "json",
        }).done(function (data) {
            console.log("Request Done!!");
            if (data['error'] == 1) {
                alert("ERROR")
            }
            else {
                //update this point to db
                console.log("Done");
            }

        }).fail(function (error) {
            console.log("error", error);
        })
    } catch (err) {
        console.log("Error Save Form", err);
    }

}